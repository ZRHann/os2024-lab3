#![no_std]
#![no_main]

#[macro_use]
extern crate log;

use core::arch::asm;
use core::fmt::Write;
use alloc::string::String;
use ysos::print;
use ysos::println;
use ysos::serial::get_serial;
use ysos::serial::get_serial_for_sure;
use ysos::*;
use ysos_kernel as ysos;
extern crate alloc;
use crate::drivers::input::INPUT_BUF;

boot::entry_point!(kernel_main);

pub fn kernel_main(boot_info: &'static boot::BootInfo) -> ! {
    ysos::init(boot_info);
    // unsafe {
    //     let ptr = 0xfffffe0000000000  as *mut u32;
    //     println!("Accessing a bad memory location: {:?}", ptr);
    //     *ptr = 42;
    //     let val = *ptr;
    //     println!("Read a bad memory location: {}", val);
    // }
    // print!("你");
    // get_serial_for_sure().send(0xE4);
    // get_serial_for_sure().send(0xBD);
    // get_serial_for_sure().send(0xA0);
    loop {
        print!("> ");
        let input = input::get_line();
        match input.trim() {
            "exit" => break,
            _ => {
                println!("You said: {}", input);
                println!("The counter value is {}", interrupt::clock::read_counter());
            }
        }
    }

    ysos::shutdown(boot_info);
}


