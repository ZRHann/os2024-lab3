use core::fmt;
use core::arch::asm;
use bitflags::bitflags;

/// A port-mapped UART 16550 serial interface.
pub struct SerialPort {
    port: u16, // 串行端口的基地址，用于访问串行端口的各个寄存器。
}

// port+1 IER
bitflags! {
    pub struct IER_Bits: u8 {
        const RECEIVED_DATA_AVAILABLE = 0x01; // 接收数据可用中断
        const TRANSMITTER_HOLDING_REGISTER_EMPTY = 0x02; // 发送保持寄存器空中断
        const RECEIVER_LINE_STATUS = 0x04; // 接收线状态中断
        const MODEM_STATUS = 0x08; // 调制解调器状态中断
    }
}

// port+2 IIR   
bitflags! {
    pub struct IIR_Bits: u8 {
        const ENABLE_FIFO = 0x01; // 启用FIFO
        const CLEAR_RECEIVE_FIFO = 0x02; // 清除接收FIFO
        const CLEAR_TRANSMIT_FIFO = 0x04; // 清除发送FIFO
        const DMA_MODE_SELECT = 0x08; // DMA模式选择
        // 16550A模型特有
        const ENABLE_64_BYTE_FIFO = 0x20; // 启用64字节FIFO（如果硬件支持）
    }
}

// port+3 LCR
bitflags! {
    pub struct LLR_Bits: u8 {
        const DATA_BITS_5 = 0x00; // 5位数据位
        const DATA_BITS_6 = 0x01; // 6位数据位
        const DATA_BITS_7 = 0x02; // 7位数据位
        const DATA_BITS_8 = 0x03; // 8位数据位
        const STOP_BITS_1 = 0x00; // 1停止位
        const STOP_BITS_2 = 0x04; // 2停止位（如果设置为5位数据位，则为1.5停止位）
        const PARITY_NONE = 0x00; // 无奇偶校验
        const PARITY_ODD = 0x08; // 奇校验
        const PARITY_EVEN = 0x18; // 偶校验
        const PARITY_MARK = 0x28; // 标记校验（始终为1）
        const PARITY_SPACE = 0x38; // 空白校验（始终为0）
        const SET_BREAK = 0x40; // 设置中断使能
        const DLAB = 0x80; // 分频锁存访问位（DLAB）
    }
}

// port+4 MCR
bitflags! {
    pub struct MCR_Bits: u8 {
        const DTR = 0x01; // 数据终端就绪
        const RTS = 0x02; // 请求发送
        const OUT1 = 0x04; // 辅助输出1
        const OUT2 = 0x08; // 辅助输出2，用于中断使能
        const LOOP = 0x10; // 环回测试模式
    }
}

// port+5 LSR
bitflags! {
    pub struct LSR_Bits: u8 {
        const DATA_READY = 0x01; // 数据准备就绪
        const OVERRUN_ERROR = 0x02; // 接收器溢出错误
        const PARITY_ERROR = 0x04; // 奇偶校验错误
        const FRAMING_ERROR = 0x08; // 帧错误
        const BREAK_INTERRUPT = 0x10; // 中断信号检测
        const TRANSMITTER_EMPTY = 0x20; // 发送保持寄存器为空
        const TRANSMITTER_HOLDING_REGISTER_EMPTY = 0x40; // 发送器空，即所有数据已发送
        const IMPENDING_ERROR = 0x80; // 在FIFO模式下，表示接收FIFO中至少有一个帧错误或溢出错误
    }
}

// 以下是UART 16550的寄存器偏移定义（假设基础端口为`port`）
const RBR: u16 = 0; // 接收缓冲寄存器(Receiver Buffer Register), 只读, 偏移0，用于读取接收到的数据
const THR: u16 = 0; // 发送保持寄存器(Transmitter Holding Register), 只写, 偏移0，用于写入发送的数据
const DLL: u16 = 0; // 分频锁存低字节(Divisor Latch Low Byte), 可读写, 偏移0，设置波特率时使用
const IER: u16 = 1; // 中断使能寄存器(Interrupt Enable Register), 可读写, 偏移1
const DLH: u16 = 1; // 分频锁存高字节(Divisor Latch High Byte), 可读写, 偏移1，设置波特率时使用
const IIR: u16 = 2; // 中断识别寄存器(Interrupt Identification Register), 只读, 偏移2，用于识别中断源
const FCR: u16 = 2; // FIFO控制寄存器(FIFO Control Register), 只写, 偏移2，用于配置FIFO
const LCR: u16 = 3; // 线控寄存器(Line Control Register), 可读写, 偏移3，用于配置数据格式
const MCR: u16 = 4; // 调制解调器控制寄存器(Modem Control Register), 可读写, 偏移4，用于控制调制解调器
const LSR: u16 = 5; // 线状态寄存器(Line Status Register), 只读, 偏移5，用于读取发送和接收状态
const MSR: u16 = 6; // 调制解调器状态寄存器(Modem Status Register), 只读, 偏移6，用于读取调制解调器状态
const SCR: u16 = 7; // 刮擦寄存器(Scratch Register), 可读写, 偏移7，备用

/// 写入一个字节到指定的端口。
///
/// # 参数
///
/// * `port` - 端口号。
/// * `value` - 要写入的字节。
unsafe fn outb(port: u16, value: u8) {
    asm!("out dx, al", in("dx") port, in("al") value, options(nomem, nostack));
}


/// 从指定的端口读取一个字节。
///
/// # 参数
///
/// * `port` - 端口号。
///
/// # 返回
///
/// 返回从端口读取的字节。
unsafe fn inb(port: u16) -> u8 {
    let value: u8;
    asm!("in al, dx", out("al") value, in("dx") port, options(nomem, nostack));
    value
}

impl SerialPort {
    pub const fn new(port: u16) -> Self {
        Self { port }
    }

    unsafe fn write_port(&self, offset: u16, value: u8) {
        unsafe {
            outb(self.port + offset, value);
        }
    }

    unsafe fn read_port(&self, offset: u16) -> u8 {
        unsafe {
            inb(self.port + offset)
        }
    }

    /// 初始化串行端口。
    pub fn init(&self) {
        unsafe {
            self.write_port(IER, IER_Bits::empty().bits()); // 禁用所有中断
            self.write_port(LCR, LLR_Bits::DLAB.bits()); // 启用DLAB
            self.write_port(DLL, 0x03); // 设置波特率低字节
            self.write_port(DLH, 0x00); // 设置波特率高字节
            self.write_port(LCR, LLR_Bits::DATA_BITS_8.bits()); // 8位数据位，无奇偶校验，1停止位
            self.write_port(FCR, IIR_Bits::ENABLE_FIFO.bits() | IIR_Bits::CLEAR_RECEIVE_FIFO.bits() | IIR_Bits::CLEAR_TRANSMIT_FIFO.bits()); // 启用并清除FIFO
            self.write_port(MCR, MCR_Bits::DTR.bits() | MCR_Bits::RTS.bits() | MCR_Bits::OUT2.bits()); // 设置MCR
            self.write_port(IER, IER_Bits::RECEIVED_DATA_AVAILABLE.bits());// 开启串口中断
        }
    }

    /// 发送一个字节到串行端口。
    pub fn send(&mut self, data: u8) {
        unsafe {
            while self.read_port(LSR) & LSR_Bits::TRANSMITTER_EMPTY.bits() == 0 {} // 等待直到传输缓冲区为空。(port+5)是LSR寄存器，第5位是THRE位，表示传输缓冲区为空。
            self.write_port(THR, data) // 将数据写入端口，发送出去。
        }
    }

    /// 无等待地从串行端口接收一个字节。
    pub fn receive(&mut self) -> Option<u8> {
        info!("Receiving data from serial port.");
        unsafe {
            if self.read_port(LSR) & LSR_Bits::DATA_READY.bits() != 0 { // 检查是否有数据可读。
                Some(self.read_port(RBR)) // 如果有，从端口读取数据。
            } else {
                None // 如果没有数据可读，返回None。
            }
        }
    }

    pub fn backspace(&mut self) {
        self.send(0x08); // 退格，光标向左移动
        self.send(0x20); // 发送空格以覆盖字符
        self.send(0x08); // 再次退格，将光标移回原位
    }
}

impl fmt::Write for SerialPort {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
            self.send(byte);
        }
        Ok(())
    }
}
