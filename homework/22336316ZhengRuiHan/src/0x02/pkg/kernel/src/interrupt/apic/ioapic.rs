// 使用bit_field库来方便地操作位字段
use bit_field::BitField;

// I/O APIC的默认物理地址
pub const IOAPIC_ADDR: u64 = 0xFEC00000;

// 使用bitflags!宏定义中断重定向条目的标志位
bitflags! {
    /// 重定向表从REG_TABLE开始，每个中断使用两个寄存器进行配置。
    /// 成对的第一个（低）寄存器包含配置位。
    /// 第二个（高）寄存器包含一个掩码，指明哪些CPU可以服务该中断。
    struct RedirectionEntry: u32 {
        /// 中断禁用
        const DISABLED  = 0x00010000;
        /// 级别触发（相对于边缘触发）
        const LEVEL     = 0x00008000;
        /// 低电平有效（相对于高电平有效）
        const ACTIVELOW = 0x00002000;
        /// 目的地是CPU id（相对于APIC ID）
        const LOGICAL   = 0x00000800;
        /// 无特别标志
        const NONE      = 0x00000000;
    }
}

// 定义IoApic结构体
pub struct IoApic {
    reg: *mut u32, // 指向寄存器的指针
    data: *mut u32, // 指向数据的指针
}

impl IoApic {
    // 创建新的IoApic实例
    pub unsafe fn new(addr: u64) -> Self {
        IoApic {
            reg: addr as *mut u32, // 将提供的地址作为寄存器的地址
            data: (addr + 0x10) as *mut u32, // 数据地址是寄存器地址之后的第16个字节
        }
    }

    // 禁用所有中断
    pub fn disable_all(&mut self) {
        // 将所有中断标记为边缘触发，高电平有效，禁用，
        // 并且不路由到任何CPU。
        for i in 0..=self.maxintr() {
            self.write_irq(i, RedirectionEntry::DISABLED, 0);
        }
    }

    // 从给定的寄存器读取数据
    unsafe fn read(&mut self, reg: u8) -> u32 {
        self.reg.write_volatile(reg as u32);
        self.data.read_volatile()
    }

    // 向给定的寄存器写入数据
    unsafe fn write(&mut self, reg: u8, data: u32) {
        self.reg.write_volatile(reg as u32); // 告诉IOAPIC, 接下来要访问的寄存器
        self.data.write_volatile(data); // 写入data, 会被转发到上面的reg寄存器
    }

    // 写入特定IRQ的配置
    fn write_irq(&mut self, irq: u8, flags: RedirectionEntry, dest: u8) {
        unsafe {
            self.write(0x10 + 2 * irq, (32 + irq) as u32 | flags.bits());
            self.write(0x10 + 2 * irq + 1, (dest as u32) << 24);
        }
    }

    // 启用特定的中断，并将其路由到指定的CPU
    pub fn enable(&mut self, irq: u8, cpuid: u8) {
        // 将中断标记为边缘触发，高电平有效，
        // 启用，并路由到给定的cpuid，
        // 这恰好是该CPU的APIC ID。
        self.write_irq(irq, RedirectionEntry::NONE, cpuid);
        trace!("Enable IOApic: IRQ={}, CPU={}", irq, cpuid);
    }

    // 禁用特定的中断
    pub fn disable(&mut self, irq: u8, cpuid: u8) {
        self.write_irq(irq, RedirectionEntry::DISABLED, cpuid);
    }

    // 获取IO APIC的ID
    pub fn id(&mut self) -> u8 {
        unsafe { self.read(0x00).get_bits(24..28) as u8 }
    }

    // 获取IO APIC的版本
    pub fn version(&mut self) -> u8 {
        unsafe { self.read(0x01).get_bits(0..8) as u8 }
    }

    // 获取IO APIC支持的最大中断号
    pub fn maxintr(&mut self) -> u8 {
        unsafe { self.read(0x01).get_bits(16..24) as u8 }
    }
}
