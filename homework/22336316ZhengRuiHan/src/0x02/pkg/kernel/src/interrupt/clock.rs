// 引入定义好的常量，可能包括中断向量号等
use super::consts::*;
use core::sync::atomic::{AtomicU64, Ordering};
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};

// 定义一个函数，用于向IDT（中断描述符表）注册时钟中断处理程序
pub unsafe fn register_idt(idt: &mut InterruptDescriptorTable) {
    // 计算时钟中断的向量号并为其设置处理函数
    idt[Interrupts::IrqBase as u8 + Irq::Timer as u8]
        .set_handler_fn(clock_handler);
}

// 定义时钟中断的处理函数
pub extern "x86-interrupt" fn clock_handler(_sf: InterruptStackFrame) {
    // 确保在执行中断处理程序时，其他中断被禁止，以避免中断嵌套引发的问题
    x86_64::instructions::interrupts::without_interrupts(|| {
        // 增加计数器的值，如果达到了特定的阈值（这里是0x10000），则记录一次tick
        info!("Tick! @{}", inc_counter());
        // if inc_counter() % 0x10000 == 0 {
        //     info!("Tick! @{}", read_counter());
        // }
        // 发送结束信号给APIC，以允许后续的中断
        super::ack();
    });
}

// 使用AtomicU64定义COUNTER，初始值为0，确保原子操作
static COUNTER: AtomicU64 = AtomicU64::new(0);

// 读取COUNTER当前值
#[inline]
pub fn read_counter() -> u64 {
    COUNTER.load(Ordering::Relaxed)
}

// 增加COUNTER值，并返回新值
#[inline]
pub fn inc_counter() -> u64 {
    // 同样使用Relaxed顺序进行原子增加操作
    COUNTER.fetch_add(1, Ordering::Relaxed) + 1
}
