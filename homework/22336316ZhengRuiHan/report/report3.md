**操作系统原理 实验三（RUST）**

## 个人信息

【院系】计算机学院

【专业】计算机科学与技术

【学号】22336316

【姓名】郑锐涵

## 实验题目

中断处理

## 实验目的

1. 了解中断的作用、中断的分类、中断的处理过程。
2. 启用基于 APIC 的中断，注册 IDT 中断处理程序，实现时钟中断。
3. 注册内核堆分配器。（不实现内存分配算法，使用现有代码赋予内核堆分配能力）
4. 实现串口驱动的输入能力，尝试进行基础的 IO 操作和交互。

## 实验要求

1. GDT 与 TSS
2. 注册中断处理程序
3. 初始化 APIC
4. 时钟中断
5. 串口输入中断
6. 用户交互
7. 思考题
8. 加分项

## 实验过程

### GDT 与 TSS

```rust
lazy_static! {
    // TSS的类型为TaskStateSegment，这里进行初始化
    static ref TSS: TaskStateSegment = {
        // 新建一个TSS实例
        let mut tss = TaskStateSegment::new();

        // 初始化TSS的特权栈表，这里仅设置了第0个栈
        // 将会在内核加载时在BSS段分配静态缓冲区
        tss.privilege_stack_table[0] = {
            // 定义栈的大小
            const STACK_SIZE: usize = IST_SIZES[0];
            // 静态分配栈空间，初始值全为0
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            // 获取栈起始地址
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            // 计算栈结束地址
            let stack_end = stack_start + STACK_SIZE as u64;
            // 打印栈的起始和结束地址
            info!(
                "Privilege Stack  : 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            // 将栈结束地址作为栈顶地址返回
            stack_end
        };

        // 为Double Fault设置独立栈
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = IST_SIZES[DOUBLE_FAULT_IST_INDEX as usize];
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            let stack_end = stack_start + STACK_SIZE as u64;
            info!(
                "Double Fault Stack: 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            stack_end
        };

        // 为Page Fault设置独立栈
        tss.interrupt_stack_table[PAGE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = IST_SIZES[PAGE_FAULT_IST_INDEX as usize];
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            let stack_end = stack_start + STACK_SIZE as u64;
            info!(
                "Page Fault Stack : 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            stack_end
        };
        // 返回初始化好的TSS实例
        tss
    };
}
```

### 注册中断处理程序

```rust
    // 设置通用保护故障的处理程序(GPF)。
    idt.general_protection_fault
        .set_handler_fn(general_protection_fault_handler)
        .set_stack_index(gdt::GENERAL_PROTECTION_FAULT_IST_INDEX);
```

```rust
// 通用保护故障的处理程序(GPF)。
pub extern "x86-interrupt" fn general_protection_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: u64, // 错误码参数
) {
    panic!(
        "EXCEPTION: GENERAL PROTECTION FAULT, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}
```

### 初始化 APIC

```rust
use super::LocalApic;
use bit_field::BitField;
use core::fmt::{Debug, Error, Formatter};
use core::ptr::{read_volatile, write_volatile};
use x86::cpuid::CpuId;
use crate::interrupt::consts::{Interrupts, Irq};
use crate::memory::physical_to_virtual;
impl XApic {
    // 创建一个新的XApic实例，自动将物理地址转换为虚拟地址
    pub unsafe fn new() -> Self {
        let virtual_addr = physical_to_virtual(LAPIC_ADDR);
        XApic { addr: virtual_addr }
    }
}

// 实现LocalApic trait，为XApic提供APIC相关的操作
impl LocalApic for XApic {
    // 检查当前系统是否支持xAPIC
    fn support() -> bool {
        let cpuid = CpuId::new();
        match cpuid.get_feature_info() {
            Some(feat_info) => feat_info.has_apic(),
            None => false,
        }
    }

    // 初始化当前CPU的xAPIC
    fn cpu_init(&mut self) {
        unsafe {
            // 启用本地APIC；设置spurious中断向量
            let mut spiv = self.read(0xF0);
            spiv |= 1 << 8; // set EN bit
            spiv &= !(0xFF);
            spiv |= Interrupts::IrqBase as u32 + Irq::Spurious as u32;
            self.write(0xF0, spiv);

            // 设置定时器以按总线频率重复计数
            let mut lvt_timer = self.read(0x320);
            lvt_timer &= !(0xFF);
            lvt_timer |= Interrupts::IrqBase as u32 + Irq::Timer as u32;
            lvt_timer &= !(1 << 16); // clear Mask
            lvt_timer |= 1 << 17; // set Timer Periodic Mode
            self.write(0x320, lvt_timer);

            // 禁用逻辑中断线（LINT0，LINT1）
            self.write(0x350, 1 << 16); // Mask LINT0
            self.write(0x360, 1 << 16); // Mask LINT1

            // 禁用性能计数器溢出中断（PCINT）
            self.write(0x340, self.read(0x340) | (1 << 16)); // Mask PCINT

            // 将错误中断映射到IRQ_ERROR
            let irq_error = Interrupts::IrqBase as u32 + Irq::Error as u32; // 使用Irq::Error来获取错误中断向量号
            self.write(0x370, irq_error); // 写入LVT错误寄存器

            // 清除错误状态寄存器（需要连续写入两次）
            self.write(0x280, 0);
            self.write(0x280, 0);

            // 确认任何未处理的中断
            self.write(0xB0, 0); // 向EOI寄存器写入0以确认任何挂起的中断

            // 发送一个Init Level De-Assert来同步仲裁ID
            self.write(0x310, 0); // set ICR 0x310
            const BCAST: u32 = 1 << 19;
            const INIT: u32 = 5 << 8;
            const TMLV: u32 = 1 << 15; // TM = 1, LV = 0
            self.write(0x300, BCAST | INIT | TMLV); // set ICR 0x300
            const DS: u32 = 1 << 12;
            while self.read(0x300) & DS != 0 {} // wait for delivery status

            // 在APIC上启用中断（但不是在处理器上）
            self.write(0x80, 0); // 通过设置TPR寄存器为0来允许接收所有优先级的中断
        }
    }
}
```

### 时钟中断

```rust
// 定义时钟中断的处理函数
pub extern "x86-interrupt" fn clock_handler(_sf: InterruptStackFrame) {
    // 确保在执行中断处理程序时，其他中断被禁止，以避免中断嵌套引发的问题
    x86_64::instructions::interrupts::without_interrupts(|| {
        // 增加计数器的值，如果达到了特定的阈值（这里是0x10000），则记录一次tick
        info!("Tick! @{}", inc_counter());
        // if inc_counter() % 0x10000 == 0 {
        //     info!("Tick! @{}", read_counter());
        // }
        // 发送结束信号给APIC，以允许后续的中断
        super::ack();
    });
}

// 使用AtomicU64定义COUNTER，初始值为0，确保原子操作
static COUNTER: AtomicU64 = AtomicU64::new(0);

// 读取COUNTER当前值
#[inline]
pub fn read_counter() -> u64 {
    COUNTER.load(Ordering::Relaxed)
}

// 增加COUNTER值，并返回新值
#[inline]
pub fn inc_counter() -> u64 {
    // 同样使用Relaxed顺序进行原子增加操作
    COUNTER.fetch_add(1, Ordering::Relaxed) + 1
}
```

![image-20240408143014103](report3.assets/image-20240408143014103.png)

**如果想要中断的频率减半，应该如何修改？**

使用分频寄存器 `Divide Configuration Register (for Timer)`（0x3E0）。

在cpu_init中：

```rust
info!("APIC Timer Divisor: {:#X}", self.read(0x3E0));
```

经过log，其初始值为0x0.

寄存器值和相应的分频值：

```
000: Divide by 2
001: Divide by 4
010: Divide by 8
011: Divide by 16
100: Divide by 32
101: Divide by 64
110: Divide by 128
111: Divide by 1
```

减半则设置成001.

```rust
// 写入分频配置寄存器
self.write(0x3E0, 0x1);
```

![image-20240408150033120](report3.assets/image-20240408150033120.png)

很明显观察到频率减半。



### 串口输入中断

```rust
    // uart16550.rs
    /// 初始化串行端口。
    pub fn init(&self) {
        unsafe {
            self.write_port(IER, IER_Bits::empty().bits()); // 禁用所有中断
            self.write_port(LCR, LLR_Bits::DLAB.bits()); // 启用DLAB
            self.write_port(DLL, 0x03); // 设置波特率低字节
            self.write_port(DLH, 0x00); // 设置波特率高字节
            self.write_port(LCR, LLR_Bits::DATA_BITS_8.bits()); // 8位数据位，无奇偶校验，1停止位
            self.write_port(FCR, IIR_Bits::ENABLE_FIFO.bits() | IIR_Bits::CLEAR_RECEIVE_FIFO.bits() | IIR_Bits::CLEAR_TRANSMIT_FIFO.bits()); // 启用并清除FIFO
            self.write_port(MCR, MCR_Bits::DTR.bits() | MCR_Bits::RTS.bits() | MCR_Bits::OUT2.bits()); // 设置MCR
            self.write_port(IER, IER_Bits::RECEIVED_DATA_AVAILABLE.bits());// 开启串口中断
        }
    }
```

```rust
	// interrupt/mod.rs
	if XApic::support() {
        let mut apic = unsafe { XApic::new(LAPIC_ADDR) };
        apic.cpu_init(); // 初始化当前CPU的APIC
        // 启用特定的IRQ
        enable_irq(Irq::Serial0 as u8, 0); // enable IRQ4 for CPU0, 启用串口中断IRQ.
    } else {
        panic!("APIC not supported, cannot initialize interrupts.");
    }
```

```rust
// input.rs
// 从缓冲区中阻塞取出数据，循环等待直到缓冲区中有数据可取
pub fn pop_key() -> Key {
    loop {
        if let Some(key) = try_pop_key() {
            return key;
        }
        // 在实际的操作系统中，这里可能需要让出CPU时间，等待下一个中断
    }
}

// 从缓冲区中获取一行数据，实时打印每个字符，直到遇到换行符 '\n'
pub fn get_line() -> String {
    let mut line = String::with_capacity(128);
    let mut serial_port = get_serial().expect("Failed to get serial port");
    loop {
        let key = pop_key(); // 阻塞等待，直到缓冲区中有数据
        match key {
            b'\n' => {
                println!(); // 当遇到换行符时，在屏幕上打印一个新行
                break;
            },
            b'\x08' | b'\x7F' => { // 处理退格键
                if !line.is_empty() {
                    line.pop(); // 移除字符串的最后一个字符
                    serial_port.backspace(); // 从屏幕上删除一个字符
                }
            },
            key => {
                if key.is_ascii() {
                    let c = key as char;
                    line.push(c);
                    print!("{}", c); // 使用print!而不是println!，以避免自动添加新行
                }
            },
        }
    }
    line
}
```

```rust
// interrupt/serial.rs
fn receive() {
    // 尝试获取串口的锁
    if let Some(mut serial_port) = get_serial() {
        // 试图从串口接收一个字节
        while let Some(data) = serial_port.receive() {
            push_key(data); // 成功读取到数据，使用 push_key 将其放入输入缓冲区
            // 因为 push_key 内部已经处理了缓冲区满的情况，这里不需要额外的错误处理
        }
        // 注意：这里没有处理接收错误的情况
    }
}
```

```rust
    // uart16550.rs
    /// 从屏幕上删除一个字符。
    pub fn backspace(&mut self) {
        self.send(0x08); // 发送退格字符
        self.send(0x20); // 发送空格以覆盖原位置的字符
        self.send(0x08); // 再次发送退格字符以将光标移回
    }
```



### 用户交互

没有按照预期工作。

在main中 `info!("INPUT_BUF length: {}", INPUT_BUF.len());`，定位到get_line可能死循环。
在get_line中 `info!("Key: {:?}", key);`，发现回车时打印了13（\r）。可能该环境下没有\n。

修改跳出循环的条件为\r。

同时在backspace前才get_serial_for_sure，避免竞争。

```rust
// 从缓冲区中获取一行数据，实时打印每个字符，直到遇到换行符 '\n'
pub fn get_line() -> String {
    let mut line = String::with_capacity(128);
    loop {
        let key = pop_key(); // 阻塞等待，直到缓冲区中有数据
        match key {
            b'\n' | b'\r' => {
                println!(); // 当遇到换行符时，在屏幕上打印一个新行
                break;
            },
            b'\x08' | b'\x7F' => { // 处理退格键
                if !line.is_empty() {
                    line.pop(); // 移除字符串的最后一个字符
                    let mut serial_port = get_serial_for_sure();
                    serial_port.backspace(); // 从屏幕上删除一个字符
                }
            },
            key => {
                if key.is_ascii() {
                    let c = key as char;
                    line.push(c);
                    print!("{}", c);
                }
            },
        }
    }
    line
}
```

![image-20240408232238408](report3.assets/image-20240408232238408.png)

并且可以退格。



### 思考题

**为什么需要在 `clock_handler` 中使用 `without_interrupts` 函数？如果不使用它，可能会发生什么情况？**

确保在执行时钟中断处理程序时，其他的中断被暂时禁止，防止中断处理程序被其他中断打断，导致出现中断嵌套的情况.



**考虑时钟中断进行进程调度的场景，时钟中断的频率应该如何设置？太快或太慢的频率会带来什么问题？请分别回答。**

过快的频率：增加中断的开销，增加进程切换的开销，更大的资源竞争风险。

过慢的频率：降低系统响应时间，减少多进程并发性，资源负载不均衡。



**在进行 `receive` 操作的时候，为什么无法进行日志输出？如果强行输出日志，会发生什么情况？谈谈你对串口、互斥锁的认识。**

输出日志最终调用的是如下函数：

```rust
#[doc(hidden)]
pub fn print_internal(args: Arguments) {
    interrupts::without_interrupts(|| {
        if let Some(mut serial) = get_serial() {
            serial.write_fmt(args).unwrap();
        }
    });
}
```

`receive`的时候，获取了锁，故输出日志的时候，`get_serial()`会返回None，导致没有输出。

强行输出可能会竞争，导致不按预期工作。



**输入缓冲区在什么情况下会满？如果缓冲区满了，用户输入的数据会发生什么情况？**

输入数据过快，导致pop_key跟不上push_key。

缓冲区满了，则超出缓冲区的部分会丢失。



**尝试用你的方式触发 Triple Fault，开启 `intdbg` 对应的选项，在 QEMU 中查看调试信息，分析 Triple Fault 的发生过程。**

```rust
lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        unsafe {
            // exceptions::register_idt(&mut idt);
            // clock::register_idt(&mut idt);
            serial::register_idt(&mut idt);
        }
        idt
    };
}
```

注释掉exceptions和clock的idt注册。当触发clock中断时，由于找不到idt条目，触发了GPF（General Protection Fault），由于exceptions也被注释，同样找不到GPF的idt，触发double fault。同理触发triple fault。

调试信息：

```
[INFO] YatSenOS initialized.
> Servicing hardware INT=0x20
   729: v=20 e=0000 i=0 cpl=0 IP=0008:ffffff000005a690 pc=ffffff000005a690 SP=0000:ffffff01001ffe98 env->regs[R_EAX]=ffffff00000c3080
RAX=ffffff00000c3080 RBX=ffffff00000c3080 RCX=ffffff00000c3200 RDX=0000000000000000
RSI=ffffff00000c3300 RDI=ffffff00000c3080 RBP=ffffff00000b7060 RSP=ffffff01001ffe98
R8 =0000000000000100 R9 =0000000000000000 R10=0000000000000001 R11=ffffff000004194f
R12=ffffff01001ffec4 R13=ffffff000009ca90 R14=ffffff01001fff00 R15=ffffff01001ffed0
RIP=ffffff000005a690 RFL=00000286 [--S--P-] CPL=0 II=0 A20=1 SMM=0 HLT=0
ES =0000 0000000000000000 00000000 00000000
CS =0008 0000000000000000 ffffffff 00af9b00 DPL=0 CS64 [-RA]
SS =0000 0000000000000000 00000000 00000000
DS =0010 0000000000000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
FS =0000 0000000000000000 00000000 00000000
GS =0000 0000000000000000 00000000 00000000
LDT=0000 0000000000000000 0000ffff 00008200 DPL=0 LDT
TR =0018 ffffff00008c6b7c 00000067 00008900 DPL=0 TSS64-avl
GDT=     ffffff00008c6bf8 00000027
IDT=     ffffff00008c32e0 00000fff
CR0=80010033 CR2=0000000000000000 CR3=0000000005c01000 CR4=00000668
DR0=0000000000000000 DR1=0000000000000000 DR2=0000000000000000 DR3=0000000000000000 
DR6=00000000ffff0ff0 DR7=0000000000000400
CCS=0000000000000084 CCD=ffffff01001ffe78 CCO=EFLAGS
EFER=0000000000000d00
check_exception old: 0xffffffff new 0xb
   730: v=0b e=0202 i=0 cpl=0 IP=0008:ffffff000005a690 pc=ffffff000005a690 SP=0000:ffffff01001ffe98 env->regs[R_EAX]=ffffff00000c3080
RAX=ffffff00000c3080 RBX=ffffff00000c3080 RCX=ffffff00000c3200 RDX=0000000000000000
RSI=ffffff00000c3300 RDI=ffffff00000c3080 RBP=ffffff00000b7060 RSP=ffffff01001ffe98
R8 =0000000000000100 R9 =0000000000000000 R10=0000000000000001 R11=ffffff000004194f
R12=ffffff01001ffec4 R13=ffffff000009ca90 R14=ffffff01001fff00 R15=ffffff01001ffed0
RIP=ffffff000005a690 RFL=00000286 [--S--P-] CPL=0 II=0 A20=1 SMM=0 HLT=0
ES =0000 0000000000000000 00000000 00000000
CS =0008 0000000000000000 ffffffff 00af9b00 DPL=0 CS64 [-RA]
SS =0000 0000000000000000 00000000 00000000
DS =0010 0000000000000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
FS =0000 0000000000000000 00000000 00000000
GS =0000 0000000000000000 00000000 00000000
LDT=0000 0000000000000000 0000ffff 00008200 DPL=0 LDT
TR =0018 ffffff00008c6b7c 00000067 00008900 DPL=0 TSS64-avl
GDT=     ffffff00008c6bf8 00000027
IDT=     ffffff00008c32e0 00000fff
CR0=80010033 CR2=0000000000000000 CR3=0000000005c01000 CR4=00000668
DR0=0000000000000000 DR1=0000000000000000 DR2=0000000000000000 DR3=0000000000000000 
DR6=00000000ffff0ff0 DR7=0000000000000400
CCS=0000000000000084 CCD=ffffff01001ffe78 CCO=EFLAGS
EFER=0000000000000d00
check_exception old: 0xb new 0xb
   731: v=08 e=0000 i=0 cpl=0 IP=0008:ffffff000005a690 pc=ffffff000005a690 SP=0000:ffffff01001ffe98 env->regs[R_EAX]=ffffff00000c3080
RAX=ffffff00000c3080 RBX=ffffff00000c3080 RCX=ffffff00000c3200 RDX=0000000000000000
RSI=ffffff00000c3300 RDI=ffffff00000c3080 RBP=ffffff00000b7060 RSP=ffffff01001ffe98
R8 =0000000000000100 R9 =0000000000000000 R10=0000000000000001 R11=ffffff000004194f
R12=ffffff01001ffec4 R13=ffffff000009ca90 R14=ffffff01001fff00 R15=ffffff01001ffed0
RIP=ffffff000005a690 RFL=00000286 [--S--P-] CPL=0 II=0 A20=1 SMM=0 HLT=0
ES =0000 0000000000000000 00000000 00000000
CS =0008 0000000000000000 ffffffff 00af9b00 DPL=0 CS64 [-RA]
SS =0000 0000000000000000 00000000 00000000
DS =0010 0000000000000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
FS =0000 0000000000000000 00000000 00000000
GS =0000 0000000000000000 00000000 00000000
LDT=0000 0000000000000000 0000ffff 00008200 DPL=0 LDT
TR =0018 ffffff00008c6b7c 00000067 00008900 DPL=0 TSS64-avl
GDT=     ffffff00008c6bf8 00000027
IDT=     ffffff00008c32e0 00000fff
CR0=80010033 CR2=0000000000000000 CR3=0000000005c01000 CR4=00000668
DR0=0000000000000000 DR1=0000000000000000 DR2=0000000000000000 DR3=0000000000000000 
DR6=00000000ffff0ff0 DR7=0000000000000400
CCS=0000000000000084 CCD=ffffff01001ffe78 CCO=EFLAGS
EFER=0000000000000d00
check_exception old: 0x8 new 0xb
Triple fault
```

可以看到三个 `check_exception old: 0x8 new 0xb`后，触发了Triple fault。0xb正是GPF。



**尝试触发 Double Fault，观察 Double Fault 的发生过程，尝试通过调试器定位 Double Fault 发生时使用的栈是否符合预期。**

注释掉clock和GPF的idt注册代码。发生了double fault。

```
[ERROR] ERROR: panic!

PanicInfo {
    payload: Any { .. },
    message: Some(
        EXCEPTION: DOUBLE FAULT, ERROR_CODE: 0x0000000000000000
        
        InterruptStackFrame {
            instruction_pointer: VirtAddr(
                0xffffff000005a740,
            ),
            code_segment: SegmentSelector {
                index: 1,
                rpl: Ring0,
            },
            cpu_flags: RFlags(
                INTERRUPT_FLAG | 0x2,
            ),
            stack_pointer: VirtAddr(
                0xffffff01001ffe98,
            ),
            stack_segment: SegmentSelector {
                index: 0,
                rpl: Ring0,
            },
        },
    ),
    location: Location {
        file: "pkg/kernel/src/interrupt/exceptions.rs",
        line: 44,
        col: 5,
    },
    can_unwind: true,
    force_no_backtrace: false,
}
```

预期情况下，发生Double Fault时，处理器将栈顶地址设置到TSS 的对应 IST 项中，即 `tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize]`，这为double fault分配了一个独立的栈，保证了异常处理程序有一个可靠的环境运行。

```
[INFO] Double Fault Stack: 0xffffff00008c7c50-0xffffff00008c8c50
```

断点到 `double_fault_handler`：

![image-20240409182000357](report3.assets/image-20240409182000357.png)

![image-20240409182141612](report3.assets/image-20240409182141612.png)

rsp是0xffffff00008c8b08，确实在Double Fault Stack内，符合预期。



**通过访问非法地址触发 Page Fault，观察 Page Fault 的发生过程。分析 Cr2 寄存器的值，并尝试回答为什么 Page Fault 属于可恢复的异常。**

尝试在main里访问0x1：

```rust
    unsafe {
        let ptr = 0x1 as *mut u32;
        println!("Accessing a bad memory location: {:?}", ptr);
        *ptr = 42;
        let val = *ptr;
        println!("Read a bad memory location: {}", val);
    }
```

但是没有触发page fault，甚至打印出了42.

查看boot的日志，发现 `physical_memory_offset: 0xffff800000000000`，访问 `0xfffffe0000000000`成功触发了page fault。

![image-20240409190047769](report3.assets/image-20240409190047769.png)

CR2里的值，用intdbg查看，是 `CR2=fffffe0000000000`。

![image-20240409213527196](report3.assets/image-20240409213527196.png)

CR2寄存器在x86架构中用于存储触发Page Fault异常时访问的虚拟地址。也符合预期。

Page Fault可以被视为一种可恢复的异常，是因为操作系统捕获到异常后，处理程序可以访问CR2寄存器的值，加载缺失页面/调整页面权限，在不终止程序执行的情况下恢复正常操作。



**如果在 TSS 中为中断分配的栈空间不足，会发生什么情况？请分析 CPU 异常的发生过程，并尝试回答什么时候会发生 Triple Fault。**

发生异常时，CPU保存当前状态，包括程序计数器、标志寄存器等保存到当前任务的栈中。对于有独立栈的异常（如double_fault，page_fault，general_protection_fault），将栈指针设置到其TSS对应的栈。然后跳转到异常处理程序。

如果在 TSS 中为中断分配的栈空间不足，则触发double fault。在double fault中如TSS 中为中断分配的栈空间仍不足，则触发Triple Fault，导致CPU重启。

Triple Fault的触发条件是CPU尝试处理double fault但失败（在double fault 处理过程中遇到其它异常）。



**在未使用 `set_stack_index` 函数时，中断处理程序的栈可能哪里？尝试结合 gdb 调试器，找到中断处理程序的栈，并验证你的猜想是否正确。**

未使用 `set_stack_index` 函数，则不改变栈指针，中断处理程序将使用当前程序的栈。

关闭页故障的 `set_stack_index` ，然后触发page fault。在触发前下断点：

![image-20240409235551221](report3.assets/image-20240409235551221.png)

观察到rsp为 `0xffffff01001fff48`：

![image-20240409235726255](report3.assets/image-20240409235726255.png)

中断里打印的stack pointer： `0xffffff01001fff48`：

![image-20240410000253944](report3.assets/image-20240410000253944.png)

与触发page fault前的rsp相同。验证了猜想正确。



### 加分项

**😋 为全部可能的 CPU 异常设置对应的处理程序，使用 `panic!` 输出异常信息。**

在idt.rs库文件中可以查看到所有中断向量表api及详细说明。

在现代x86架构中，"Coprocessor Segment Overrun"异常（中断向量0x09）已经被认为是过时的，实际上在现代CPU中不再使用。这个异常原本是在早期的x86处理器中使用的，用于处理浮点协处理器（如8087和80287）的段溢出错误。

完整代码：

```rust
// 引入memory模块，这个模块可能包含一些关于内存管理的定义或函数。
use crate::memory::*;

// 引入x86_64包中的Cr2寄存器相关的功能，Cr2寄存器在发生页故障时存储引发故障的地址。
use x86_64::registers::control::Cr2;

// 引入x86_64包中的IDT（中断描述表）相关的结构体和枚举。
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode};
use x86_64::VirtAddr;

// 定义一个函数用于注册中断处理程序到IDT。
pub unsafe fn register_idt(idt: &mut InterruptDescriptorTable) {
    idt.divide_error.set_handler_fn(divide_error_handler); // 0x00
    idt.debug.set_handler_fn(debug_handler); // 0x01
    idt.non_maskable_interrupt.set_handler_fn(non_maskable_interrupt_handler); // 0x02
    idt.breakpoint.set_handler_fn(breakpoint_handler); // 0x03
    idt.overflow.set_handler_fn(overflow_handler); // 0x04
    idt.bound_range_exceeded.set_handler_fn(bound_range_exceeded_handler); // 0x05
    idt.invalid_opcode.set_handler_fn(invalid_opcode_handler); // 0x06
    idt.device_not_available.set_handler_fn(device_not_available_handler); // 0x07
    idt.double_fault.set_handler_fn(double_fault_handler)
        .set_stack_index(gdt::DOUBLE_FAULT_IST_INDEX); // 0x08
    idt.invalid_tss.set_handler_fn(invalid_tss_handler); // 0x0A
    idt.segment_not_present.set_handler_fn(segment_not_present_handler); // 0x0B
    idt.stack_segment_fault.set_handler_fn(stack_segment_fault_handler); // 0x0C
    idt.general_protection_fault.set_handler_fn(general_protection_fault_handler)
        .set_stack_index(gdt::GENERAL_PROTECTION_FAULT_IST_INDEX); // 0x0D
    idt.page_fault.set_handler_fn(page_fault_handler)
        .set_stack_index(gdt::PAGE_FAULT_IST_INDEX); // 0x0E
    idt.x87_floating_point.set_handler_fn(x87_floating_point_handler); // 0x10
    idt.alignment_check.set_handler_fn(alignment_check_handler); // 0x11
    idt.machine_check.set_handler_fn(machine_check_handler); // 0x12
    idt.simd_floating_point.set_handler_fn(simd_floating_point_handler); // 0x13
    idt.virtualization.set_handler_fn(virtualization_handler); // 0x14
    idt.cp_protection_exception.set_handler_fn(cp_protection_exception_handler); // 0x15
    idt.hv_injection_exception.set_handler_fn(hv_injection_exception_handler); // 0x1C
    idt.vmm_communication_exception.set_handler_fn(vmm_communication_exception_handler); // 0x1D
    idt.security_exception.set_handler_fn(security_exception_handler); // 0x1E
}


pub extern "x86-interrupt" fn divide_error_handler(stack_frame: InterruptStackFrame) { // 0x00
    panic!("EXCEPTION: DIVIDE ERROR\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn debug_handler(stack_frame: InterruptStackFrame) { // 0x01
    panic!("EXCEPTION: DEBUG\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn non_maskable_interrupt_handler(stack_frame: InterruptStackFrame) { // 0x02
    panic!("EXCEPTION: NON-MASKABLE INTERRUPT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn breakpoint_handler(stack_frame: InterruptStackFrame) { // 0x03
    panic!("EXCEPTION: BREAKPOINT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn overflow_handler(stack_frame: InterruptStackFrame) { // 0x04
    panic!("EXCEPTION: OVERFLOW\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn bound_range_exceeded_handler(stack_frame: InterruptStackFrame) { // 0x05
    panic!("EXCEPTION: BOUND RANGE EXCEEDED\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn invalid_opcode_handler(stack_frame: InterruptStackFrame) { // 0x06
    panic!("EXCEPTION: INVALID OPCODE\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn device_not_available_handler(stack_frame: InterruptStackFrame) { // 0x07
    panic!("EXCEPTION: DEVICE NOT AVAILABLE\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn double_fault_handler( // 0x08
    stack_frame: InterruptStackFrame,
    error_code: u64,
) -> ! {
    panic!(
        "EXCEPTION: DOUBLE FAULT, ERROR_CODE: 0x{:016x}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn invalid_tss_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x0A
    panic!(
        "EXCEPTION: INVALID TSS, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn segment_not_present_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x0B
    panic!(
        "EXCEPTION: SEGMENT NOT PRESENT, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn stack_segment_fault_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x0C
    panic!(
        "EXCEPTION: STACK-SEGMENT FAULT, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn general_protection_fault_handler( // 0x0D
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    panic!(
        "EXCEPTION: GENERAL PROTECTION FAULT, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn page_fault_handler( // 0x0E
    stack_frame: InterruptStackFrame,
    err_code: PageFaultErrorCode,
) {
    panic!(
        "EXCEPTION: PAGE FAULT, ERROR_CODE: {:?}\n\nTrying to access: {:#x}\n{:#?}",
        err_code,
        Cr2::read().unwrap_or(VirtAddr::new_truncate(0xdeadbeef)),
        stack_frame
    );
}

pub extern "x86-interrupt" fn x87_floating_point_handler(stack_frame: InterruptStackFrame) { // 0x10
    panic!("EXCEPTION: x87 FLOATING POINT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn alignment_check_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x11
    panic!(
        "EXCEPTION: ALIGNMENT CHECK, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn machine_check_handler(stack_frame: InterruptStackFrame) -> ! { // 0x12
    panic!("EXCEPTION: MACHINE CHECK\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn simd_floating_point_handler(stack_frame: InterruptStackFrame) { // 0x13
    panic!("EXCEPTION: SIMD FLOATING POINT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn virtualization_handler(stack_frame: InterruptStackFrame) { // 0x14
    panic!("EXCEPTION: VIRTUALIZATION\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn cp_protection_exception_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x15
    panic!(
        "EXCEPTION: COPROCESSOR PROTECTION EXCEPTION, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn hv_injection_exception_handler(stack_frame: InterruptStackFrame) { // 0x1C
    panic!("EXCEPTION: HV INJECTION EXCEPTION\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn vmm_communication_exception_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x1D
    panic!(
        "EXCEPTION: VMM COMMUNICATION EXCEPTION, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn security_exception_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x1E
    panic!(
        "EXCEPTION: SECURITY EXCEPTION, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}
```



**😋 你如何定义用于计数的 `COUNTER`，它能够做到线程安全吗？如果不能，如何修改？**

```rust
static COUNTER: AtomicU64 = AtomicU64::new(0);
```

对它的操作是原子的，线程安全。



**🤔 操作 APIC 时存在大量比特操作，尝试结合使用 `bitflags` 和 `bit_field` 来定义和操作这些寄存器的值，从而获得更好的可读性。**

完整代码：

```rust
use super::LocalApic;
use bit_field::BitField;
use core::fmt::{Debug, Error, Formatter};
use core::ptr::{read_volatile, write_volatile};
use x86::cpuid::CpuId;
use crate::interrupt::consts::{Interrupts, Irq};

// 默认的xAPIC物理地址常量
pub const LAPIC_ADDR: u64 = 0xFEE00000;

// https://wiki.osdev.org/APIC
bitflags! {
    struct LapicRegister: u32 {
        const LAPIC_ID = 0x020; // LAPIC ID Register
        const LAPIC_VERSION = 0x030; // LAPIC Version Register
        const TPR = 0x080; // Task Priority Register
        const APR = 0x090; // Arbitration Priority Register
        const PPR = 0x0A0; // Processor Priority Register
        const EOI = 0x0B0; // EOI Register
        const RRD = 0x0C0; // Remote Read Register
        const LOGICAL_DEST = 0x0D0; // Logical Destination Register
        const DEST_FORMAT = 0x0E0; // Destination Format Register
        const SPIV = 0x0F0; // Spurious Interrupt Vector Register
        const ISR = 0x100; // In-Service Register (Start of ISR registers)
        const TMR = 0x180; // Trigger Mode Register (Start of TMR registers)
        const IRR = 0x200; // Interrupt Request Register (Start of IRR registers)
        const ERROR_STATUS = 0x280; // Error Status Register
        const LVT_CMCI = 0x2F0; // LVT Corrected Machine Check Interrupt Register
        const ICR_LOW = 0x300; // Interrupt Command Register Low
        const ICR_HIGH = 0x310; // Interrupt Command Register High
        const LVT_TIMER = 0x320; // LVT Timer Register
        const LVT_THERMAL_SENSOR = 0x330; // LVT Thermal Sensor Register
        const LVT_PERF_MONITORING = 0x340; // LVT Performance Monitoring Counters Register
        const LVT_LINT0 = 0x350; // LVT LINT0 Register
        const LVT_LINT1 = 0x360; // LVT LINT1 Register
        const LVT_ERROR = 0x370; // LVT Error Register
        const INITIAL_COUNT = 0x380; // Initial Count Register
        const CURRENT_COUNT = 0x390; // Current Count Register
        const DIVIDE_CONFIG = 0x3E0; // Divide Configuration Register
    }

    /// Defines flags for the Spurious Interrupt Vector Register (SPIV)
    pub struct SpivFlags: u32 {
        /// The interrupt vector for the spurious interrupt. Typically set to 0xFF.
        const VECTOR_MASK = 0xFF;
        /// Bit to enable the APIC. Set this to enable.
        const APIC_ENABLE = 0x100;
        /// If set, EOI messages are not broadcasted on handling spurious interrupts.
        const EOI_BROADCAST_SUPPRESSION = 0x1000;
    }

    pub struct LvtTimerFlags: u32 {
        const VECTOR_MASK = 0xFF;
        const MASK = 1 << 16;
        const PERIODIC = 1 << 17;
    }

    pub struct IcrLowFlags: u32 {
        const VECTOR_MASK = 0xFF;
        const DELIVERY_MODE_NORMAL = 0b000 << 8;
        const DELIVERY_MODE_LOWEST_PRIORITY = 0b001 << 8;
        const DELIVERY_MODE_SMI = 0b010 << 8;
        const DELIVERY_MODE_NMI = 0b100 << 8;
        const DELIVERY_MODE_INIT = 0b101 << 8;
        const DELIVERY_MODE_SIPI = 0b110 << 8;
        const DESTINATION_MODE_PHYSICAL = 0 << 11;
        const DESTINATION_MODE_LOGICAL = 1 << 11;
        const DELIVERY_STATUS = 1 << 12;
        // Bit 13 is reserved
        const INIT_LEVEL_DEASSERT = 0 << 14; // This and the next flag can be combined to represent bit 14 clear and bit 15 set respectively
        const LEVEL = 1 << 15;
        const DESTINATION_SHORTHAND_NONE = 0b00 << 18;
        const DESTINATION_SHORTHAND_SELF = 0b01 << 18;
        const DESTINATION_SHORTHAND_ALL_INCLUDING_SELF = 0b10 << 18;
        const DESTINATION_SHORTHAND_ALL_EXCLUDING_SELF = 0b11 << 18;
        // Bits 20-31 are reserved
    }

    pub struct IcrHighFlags: u32 {
        const DESTINATION_FIELD_MASK = 0x0F000000; // bits 24-27 are used for the APIC ID
    }
}


// 定义XApic结构体，代表一个xAPIC
pub struct XApic {
    addr: u64, // xAPIC的基地址
}

impl XApic {
    // 创建一个新的XApic实例，自动将物理地址转换为虚拟地址
    pub unsafe fn new(addr: u64) -> Self {
        XApic { addr }
    }

    // 从指定的寄存器读取一个值
    unsafe fn read(&self, reg: LapicRegister) -> u32 {
        read_volatile((self.addr + reg.bits() as u64) as *const u32)
    }

    // 向指定的寄存器写入一个值
    unsafe fn write(&mut self, reg: LapicRegister, value: u32) {
        write_volatile((self.addr + reg.bits() as u64) as *mut u32, value);
        self.read(LapicRegister::SPIV); // 读取一个寄存器, 确保写操作完成
    }
}

// 实现LocalApic trait，为XApic提供APIC相关的操作
impl LocalApic for XApic {
    // 检查当前系统是否支持xAPIC
    fn support() -> bool {
        let cpuid = CpuId::new();
        match cpuid.get_feature_info() {
            Some(feat_info) => feat_info.has_apic(),
            None => false,
        }
    }
    // 初始化当前CPU的xAPIC
    fn cpu_init(&mut self) {
        unsafe {
            // 启用本地APIC；设置spurious中断向量
            let mut spiv = SpivFlags::from_bits_truncate(self.read(LapicRegister::SPIV));
            spiv.insert(SpivFlags::APIC_ENABLE); // set EN bit
            spiv.remove(SpivFlags::VECTOR_MASK); // 清除低8位（中断向量）
            let irq_spurious_vector = Interrupts::IrqBase as u32 + Irq::Spurious as u32;
            let new_spiv_value = spiv.bits() | (irq_spurious_vector & SpivFlags::VECTOR_MASK.bits()); // 设置低8位（中断向量）
            self.write(LapicRegister::SPIV, new_spiv_value);

            // 设置定时器以按总线频率重复计数
            let mut lvt_timer_flags = LvtTimerFlags::from_bits_truncate(self.read(LapicRegister::LVT_TIMER));
            lvt_timer_flags.remove(LvtTimerFlags::VECTOR_MASK); // 清除现有的向量值
            let irq_timer_vector = Interrupts::IrqBase as u32 + Irq::Timer as u32;
            lvt_timer_flags |= LvtTimerFlags::from_bits_truncate(irq_timer_vector); // 设置新的定时器中断向量
            lvt_timer_flags.remove(LvtTimerFlags::MASK); // 清除Mask位，以启用定时器中断
            lvt_timer_flags.insert(LvtTimerFlags::PERIODIC); // 设置定时器为周期模式
            self.write(LapicRegister::LVT_TIMER, lvt_timer_flags.bits()); // 将修改后的值写回LVT Timer寄存器
            
            // 写入分频配置寄存器
            self.write(LapicRegister::DIVIDE_CONFIG, 0x1);

            // 禁用逻辑中断线（LINT0，LINT1）
            self.write(LapicRegister::LVT_LINT0, 1 << 16); // Mask LINT0
            self.write(LapicRegister::LVT_LINT1, 1 << 16); // Mask LINT1

            // 禁用性能计数器溢出中断（PCINT）
            self.write(LapicRegister::LVT_PERF_MONITORING, self.read(LapicRegister::LVT_PERF_MONITORING) | (1 << 16)); // Mask PCINT

            // 将错误中断映射到IRQ_ERROR
            let irq_error = Interrupts::IrqBase as u32 + Irq::Error as u32; // 使用Irq::Error来获取错误中断向量号
            self.write(LapicRegister::LVT_ERROR, irq_error); // 写入LVT错误寄存器

            // 清除错误状态寄存器（需要连续写入两次）
            self.write(LapicRegister::ERROR_STATUS, 0);
            self.write(LapicRegister::ERROR_STATUS, 0);

            // 确认任何未处理的中断
            self.write(LapicRegister::EOI, 0); // 向EOI寄存器写入0以确认任何挂起的中断

            // 发送一个Init Level De-Assert来同步仲裁ID
            self.write(LapicRegister::ICR_HIGH, 0); // set ICR 0x310
            let icr_low_flags = IcrLowFlags::DELIVERY_MODE_INIT | IcrLowFlags::LEVEL | IcrLowFlags::DESTINATION_SHORTHAND_ALL_INCLUDING_SELF;
            self.write(LapicRegister::ICR_LOW, icr_low_flags.bits());
            const DS: u32 = 1 << 12;
            while self.read(LapicRegister::ICR_LOW) & IcrLowFlags::DELIVERY_STATUS.bits() != 0 {} // wait for delivery status

            // 在APIC上启用中断（但不是在处理器上）
            self.write(LapicRegister::TPR, 0); // 通过设置TPR寄存器为0来允许接收所有优先级的中断
        }
    }

    // 获取当前APIC的ID
    fn id(&self) -> u32 {
        unsafe { self.read(LapicRegister::LAPIC_ID) >> 24 }
    }

    // 获取当前APIC的版本号
    fn version(&self) -> u32 {
        unsafe { self.read(LapicRegister::LAPIC_VERSION) }
    }

    // 获取当前APIC的中断命令寄存器（ICR）值
    fn icr(&self) -> u64 {
        unsafe { (self.read(LapicRegister::ICR_HIGH) as u64) << 32 | self.read(LapicRegister::ICR_LOW) as u64 }
    }

    // 设置中断命令寄存器（ICR）的值
    fn set_icr(&mut self, value: u64) {
        unsafe {
            // 等待当前的中断传递完成
            while self.read(LapicRegister::ICR_LOW).get_bit(12) {}
            // 设置ICR的高32位
            self.write(LapicRegister::ICR_HIGH, (value >> 32) as u32);
            // 设置ICR的低32位
            self.write(LapicRegister::ICR_LOW, value as u32);
            // 再次等待中断传递完成
            while self.read(LapicRegister::ICR_LOW).get_bit(12) {}
        }
    }

    // 发送一个End of Interrupt（EOI）信号
    fn eoi(&mut self) {
        unsafe {
            self.write(LapicRegister::EOI, 0);
        }
    }
}

// 实现Debug trait，为XApic提供格式化输出功能
impl Debug for XApic {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        f.debug_struct("Xapic")
         .field("id", &self.id())
         .field("version", &self.version())
         .field("icr", &self.icr())
         .finish()
    }
}
```



**🤔 你的串口输入驱动是否能正确的处理中文甚至 emoji 输入？如何能够正确处理？**

**UTF-8编码规则：**

**单字节字符**：如果一个字符是ASCII字符，其范围是0x00到0x7F，那么它在UTF-8中的表示和ASCII编码相同，占用一个字节。这意味着7位用于表示字符，最高位（第8位）设为0。

**多字节字符**：对于超出ASCII范围的字符，UTF-8使用2到4个字节来编码一个字符。这些字符的第一个字节始终以两位或更多位的1开始，随后跟着一位0，这个模式确保了它们不会与ASCII字符混淆。剩下的字节都以`10`开头。

- **2字节字符**：第一个字节以`110`开头，第二个字节以`10`开头。
- **3字节字符**：第一个字节以`1110`开头，后续字节以`10`开头。
- **4字节字符**：第一个字节以`11110`开头，后续字节以`10`开头。



经过测试，直接print中文是可以的，uart驱动将字符串拆成字节来send，同时支持了utf8编码。那么只需要修改input.rs。

![image-20240410123444055](report3.assets/image-20240410123444055.png)

在控制台输入”你“，捕获到的key是：

```
> [INFO] key: E4
ä[INFO] key: BD
½[INFO] key: A0
```

分成3次print，则无法输出"你"

![image-20240410132024132](report3.assets/image-20240410132024132.png)

而直接send 3次，则可以输出”你“：

![image-20240410132608235](report3.assets/image-20240410132608235.png)

但直接在getline里修改成send，也无法输出”你“。可能必须连续地send。

![image-20240410133114138](report3.assets/image-20240410133114138.png)

那么先将输入的字节存到一个utf8_buffer。当buffer里积累了一个合法的utf8，再一起print出来。代码如下：

```rust
// 检查字节序列是否构成了一个完整的UTF-8字符
fn is_complete_utf8_char(bytes: &[u8]) -> bool {
    match bytes.len() {
        1 => bytes[0] & 0x80 == 0, // 对于1字节的ASCII字符
        2 => bytes[0] & 0xE0 == 0xC0, // 对于2字节的字符
        3 => bytes[0] & 0xF0 == 0xE0, // 对于3字节的字符
        4 => bytes[0] & 0xF8 == 0xF0, // 对于4字节的字符
        _ => false,
    }
}

pub fn get_line() -> String {
    let mut line = String::with_capacity(128);
    let mut utf8_bytes = Vec::new();

    loop {
        let key = pop_key(); // 阻塞等待，直到缓冲区中有数据
        utf8_bytes.push(key);

        // 检查是否收集到了完整的UTF-8字符
        if is_complete_utf8_char(&utf8_bytes) {
            if let Ok(s) = core::str::from_utf8(&utf8_bytes) {
                match s {
                    "\n" | "\r" => {
                        println!();
                        break;
                    },
                    "\x08" | "\x7F" => { // 处理退格键
                        if !line.is_empty() {
                            line.pop();
                            get_serial_for_sure().backspace(); // 从屏幕上删除一个字符
                        }
                    },
                    _ => {
                        line.push_str(s);
                        print!("{}", s);
                    },
                }
            }
            utf8_bytes.clear(); // 清空缓冲区以接收下一个字符
        }
    }
    line
}
```

![image-20240410134117784](report3.assets/image-20240410134117784.png)

此外还需要处理退格的逻辑。对于全角字符应该退格2次。

UTF-8编码中，一个字符可能由1到4个字节组成，且字符的显示宽度并不总是与字节数直接相关（如全角字符和半角字符）。这里假设所有ASCII字符（0x00到0x7F）为半角，宽度为1；而所有非ASCII UTF-8字符默认为全角，宽度为2。

从line中pop时，会自动pop出来一个完整的utf8字符。此时可以用pop出的字节数来计算需要退格的次数。

代码如下：

```rust
// 检查字节序列是否构成了一个完整的UTF-8字符
fn is_complete_utf8_char(bytes: &[u8]) -> bool {
    match bytes.len() {
        1 => bytes[0] & 0x80 == 0, // 对于1字节的ASCII字符
        2 => bytes[0] & 0xE0 == 0xC0, // 对于2字节的字符
        3 => bytes[0] & 0xF0 == 0xE0, // 对于3字节的字符
        4 => bytes[0] & 0xF8 == 0xF0, // 对于4字节的字符
        _ => false,
    }
}


pub fn get_line() -> String {
    let mut line = String::with_capacity(128);
    let mut utf8_bytes = Vec::new();

    loop {
        let key = pop_key(); // 阻塞等待，直到缓冲区中有数据
        utf8_bytes.push(key);

        // 检查是否收集到了完整的UTF-8字符
        if is_complete_utf8_char(&utf8_bytes) {
            if let Ok(s) = core::str::from_utf8(&utf8_bytes) {
                match s {
                    "\n" | "\r" => {
                        println!();
                        break;
                    },
                    "\x08" | "\x7F" => { // 处理退格键
                        if !line.is_empty() {
                            let last_utf8_len = line.pop().unwrap().len_utf8();
                            let utf8_display_width = match last_utf8_len {
                                1 => 1,
                                2 => 2,
                                3 => 2,
                                4 => 2,
                                _ => 0,
                            };
                            for _ in 0..utf8_display_width {
                                get_serial_for_sure().backspace(); // 从屏幕上删除一个字符
                            }
                        }
                    },
                    _ => {
                        line.push_str(s);
                        print!("{}", s);
                    },
                }
            }
            utf8_bytes.clear(); // 清空缓冲区以接收下一个字符
        }
    }
    line
}
```



## 实验总结

Spurious中断是指没有实际硬件中断源的中断。APIC允许为这种中断指定一个向量号，以便软件可以识别并正确处理它们。通常，这也意味着将APIC设置为工作状态。

获取mut引用/锁，在使用之前再获取而不是提前获取，也许可以减少竞争的bug。

在大多数现代 x86 系统中，Triple Fault 会导致 CPU 执行重置操作，这通常意味着整个系统会重启。这是一种保护机制，用于防止系统因为无法处理的异常而陷入不确定状态。

在现代x86架构中，"Coprocessor Segment Overrun"异常（中断向量0x09）已经被认为是过时的，实际上在现代CPU中不再使用。这个异常原本是在早期的x86处理器中使用的，用于处理浮点协处理器（如8087和80287）的段溢出错误。

当发生 Double Fault 时，如果能够成功调用 Double Fault 的处理程序，系统不会重启；相反，它会尝试通过执行 Double Fault 处理程序来解决问题。

如果一个字符是ASCII字符，其范围是0x00到0x7F，那么它在UTF-8中的表示和ASCII编码相同，占用一个字节。这意味着7位用于表示字符，最高位（第8位）设为0。对于超出ASCII范围的字符，UTF-8使用2到4个字节来编码一个字符。这些字符的第一个字节始终以两位或更多位的1开始，随后跟着一位0，这个模式确保了它们不会与ASCII字符混淆。剩下的字节都以`10`开头。

通过本次实验，我了解了中断的作用、中断的分类、中断的处理过程。学会了启用基于 APIC 的中断，注册 IDT 中断处理程序，实现时钟中断。学会了注册内核堆分配器。（不实现内存分配算法，使用现有代码赋予内核堆分配能力）。还实现了串口驱动的输入能力，尝试进行基础的 IO 操作和交互。



## 参考文献

https://wiki.osdev.org/Interrupt_Vector_Table

https://wiki.osdev.org/APIC
